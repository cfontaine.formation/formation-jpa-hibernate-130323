package fr.dawan.exercicejpql.dao;

import java.util.List;

import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Nation;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class AuteurDao extends AbstractDao<Auteur> {

    private static final long serialVersionUID = 1L;

    public AuteurDao(EntityManager em) {
        super(em, Auteur.class);
    }
    
    public List<Auteur>findAlive(){
        TypedQuery<Auteur> query=em.createNamedQuery("Auteur.alive",Auteur.class);
        return query.getResultList();
    }


    public List<Auteur> findByNoBook(){
        TypedQuery<Auteur> query=em.createNamedQuery("Auteur.nobook",Auteur.class);
        return query.getResultList();
    }


    public List<Auteur> findByNation(Nation nation){
        TypedQuery<Auteur>  query=em.createNamedQuery("Auteur.byNation",Auteur.class);
        query.setParameter(1, nation);
        return query.getResultList();
    }

//SELECT a FROM Auteur a JOIN a.livres l GROUP BY a ORDER BY COUNT(l) DESC
    public List<Auteur> getTop5Auteur(){ 
        TypedQuery<Auteur>  query=em.createQuery("SELECT a FROM Auteur a  ORDER BY SIZE(a.livres) DESC", Auteur.class);
        return query.setMaxResults(5).getResultList();
    }

    public List<Auteur> getTop10AgeAuteur(){
        TypedQuery<Auteur>  query=em.createQuery("SELECT a FROM Auteur a ORDER BY a.dateNaissance", Auteur.class);
        return query.setMaxResults(10).getResultList();
    }
}
