package fr.dawan.exercicejpql.dao;

public class StatLivre {

    private long count;
    private String data;

    public StatLivre(long count, String data) {
        this.count = count;
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "StatLivre [count=" + count + ", data=" + data + "]";
    }
}
