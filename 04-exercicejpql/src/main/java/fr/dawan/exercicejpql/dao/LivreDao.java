package fr.dawan.exercicejpql.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Categorie;
import fr.dawan.exercicejpql.entities.Livre;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class LivreDao extends AbstractDao<Livre> {

    private static final long serialVersionUID = 1L;

    public LivreDao(EntityManager em) {
        super(em, Livre.class);
    }

    public List<Livre> findByAnnee(int anneeSortie) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie= :annee", Livre.class);
        query.setParameter("annee", anneeSortie);
        return query.getResultList();
    }

    public List<Livre> findByIntervalAnnee(int anneeMin, int anneeMax) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie BETWEEN :min AND :max",Livre.class);
        query.setParameter("min", anneeMin);
        query.setParameter("max", anneeMax);
        return query.getResultList();
    }

    public List<Livre> findStartWith(String prefix) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.titre LIKE CONCAT(:pre,'%')", Livre.class);
        query.setParameter("pre", prefix);
        return query.getResultList();
    }

    public long countByAnneeSortie(int anneeSortie) {
        TypedQuery<Long> query = em.createQuery("SELECT count(l) FROM Livre l WHERE l.anneeSortie=:anneeSortie", Long.class);
        query.setParameter("anneeSortie", anneeSortie);
        return query.getSingleResult();
    }

    public List<Livre> findByCategorie(Categorie categorie) {
        TypedQuery<Livre> query = em.createQuery(
                "SELECT l FROM Livre l WHERE l.categorie= :categorie ORDER BY l.anneeSortie DESC", Livre.class);
        query.setParameter("categorie", categorie);
        return query.getResultList();
    }

    public List<Livre> findByAuteur(Auteur auteur) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a WHERE a=:auteur ", Livre.class);
        query.setParameter("auteur", auteur);
        return query.getResultList();
    }

    public List<Livre> findMultiAuteur() {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a GROUP BY l HAVING COUNT(a)>1",
                Livre.class);
        return query.getResultList();
    }
    
    public List<Livre> findByAnnees(int... anneesSortie){ 
        if(anneesSortie.length>0) {
            String req="SELECT l FROM Livre l WHERE l.anneeSortie IN( ";
            req+=anneesSortie[0];
            for(int i=1;i<anneesSortie.length;i++) {
                req+="," + anneesSortie[i];
            }
            req+=" )";
            TypedQuery<Livre> query = em.createQuery(req,Livre.class);
            return query.getResultList();
        }
        return new ArrayList<>();
    }

    public List<StatLivre> getStatLivreByAuteur(){
        TypedQuery<StatLivre> query=em.createQuery("SELECT new fr.dawan.exercicejpql.dao.StatLivre(Count(l),CONCAT(a.prenom, ' ', a.nom))"
                + " FROM Livre l JOIN l.auteurs a GROUP BY a ORDER BY count(l) DESC",StatLivre.class);
        
        return query.getResultList();
    }
    
    public List<StatLivre> getStatLivreByCategorie(){
        TypedQuery<StatLivre> query=em.createQuery("SELECT new fr.dawan.exercicejpql.dao.StatLivre(COUNT(l),l.categorie.nom)"
                + " FROM Livre l GROUP BY l.categorie ORDER BY count(l) DESC",StatLivre.class);
        return query.getResultList();
    }
    
    public int getMaxLivreAnnee() {
        TypedQuery<Integer> query=em.createQuery("SELECT l.anneeSortie FROM Livre l GROUP BY l.anneeSortie ORDER BY COUNT(l) DESC",Integer.class);
        return query.setMaxResults(1).getSingleResult();
    }
    
}
