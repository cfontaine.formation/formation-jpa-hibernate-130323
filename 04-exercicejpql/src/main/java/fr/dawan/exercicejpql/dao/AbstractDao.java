package fr.dawan.exercicejpql.dao;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;

import fr.dawan.exercicejpql.entities.AbstractEntity;

public abstract class AbstractDao<T extends AbstractEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected EntityManager em;

    private Class<T> classEntity;

    public AbstractDao(EntityManager em, Class<T> classEntity) {
        this.em = em;
        this.classEntity = classEntity;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public void saveOrUpdate(T elm) throws Exception {
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            if (elm.getId() == 0) {
                em.persist(elm);
            } else {
                em.merge(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public void remove(T elm) throws Exception {
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            if (elm.getId() != 0) {
                em.remove(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public T findbyId(long id) {
        return em.find(classEntity, id);
    }

    public List<T> findAll() {
        TypedQuery<T> query = em.createQuery("SELECT e FROM " + classEntity.getName() + " e", classEntity);
        return query.getResultList();
    }

    public long count() {
        TypedQuery<Long> query = em.createQuery("SELECT COUNT(e) FROM " + classEntity.getName() + " e", Long.class);
        return query.getSingleResult();
    }

}
