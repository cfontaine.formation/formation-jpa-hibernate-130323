package fr.dawan.exercicejpql.dao;

import fr.dawan.exercicejpql.entities.Nation;
import jakarta.persistence.EntityManager;

public class NationDao extends AbstractDao<Nation>{

    private static final long serialVersionUID = 1L;
    
    public NationDao(EntityManager em) {
        super(em, Nation.class);
    }
}
