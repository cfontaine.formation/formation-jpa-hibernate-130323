package fr.dawan.exercicejpql.dao;

import fr.dawan.exercicejpql.entities.Categorie;
import jakarta.persistence.EntityManager;

public class CategorieDao extends AbstractDao<Categorie> {

    private static final long serialVersionUID = 1L;
    
    public CategorieDao(EntityManager em) {
        super(em, Categorie.class);
    }


}
