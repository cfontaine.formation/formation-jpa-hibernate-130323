package fr.dawan.exercicejpql;

import java.util.List;

import fr.dawan.exercicejpql.dao.AuteurDao;
import fr.dawan.exercicejpql.dao.CategorieDao;
import fr.dawan.exercicejpql.dao.LivreDao;
import fr.dawan.exercicejpql.dao.NationDao;
import fr.dawan.exercicejpql.dao.StatLivre;
import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Categorie;
import fr.dawan.exercicejpql.entities.Livre;
import fr.dawan.exercicejpql.entities.Nation;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class App {
    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("bibliotheque");
        EntityManager em=emf.createEntityManager();
        
        LivreDao dao=new LivreDao(em);
        CategorieDao categorieDao= new CategorieDao(em);
        AuteurDao auteurDao=new AuteurDao(em);

        List<Livre> lst=dao.findByAnnee(1954);
        for(Livre l:lst) {
            System.out.println(l);
        }
        
        lst=dao.findByIntervalAnnee(1990, 1999);
        for(Livre l :lst) {
            System.out.println(l);
        }
        
        lst=dao.findStartWith("du");
        for(Livre l :lst) {
            System.out.println(l);
        }
        
        long nbLivreByAnnee=dao.countByAnneeSortie(1992);
        System.out.println(nbLivreByAnnee);
        
        Categorie c=categorieDao.findbyId(1L);
        lst=dao.findByCategorie(c);
        System.out.println("Categorie: "+c.getNom());
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        Auteur a=auteurDao.findbyId(3L);
        lst=dao.findByAuteur(a);
        System.out.println("Auteur: "+a.getPrenom() + " "+ a.getNom());
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        lst=dao.findMultiAuteur();
        for(Livre l : lst) {
            System.out.println(l);
        }

        System.out.println(dao.count());
        System.out.println(categorieDao.count());
        System.out.println(auteurDao.count());
               
        lst=dao.findByAnnees(1954,1992,1974);
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        List<StatLivre> stat=dao.getStatLivreByAuteur();
        for(StatLivre s: stat) {
            System.out.println(s);
        }
        
        stat=dao.getStatLivreByCategorie();
        for(StatLivre s: stat) {
            System.out.println(s);
        }
        
        System.out.println(dao.getMaxLivreAnnee());
//        
        List<Auteur> lstA=auteurDao.findAlive();
        for(Auteur aut : lstA) {
            System.out.println(aut);
        }
//        
        lstA=auteurDao.findByNoBook();
        for(Auteur aut : lstA) {
            System.out.println(aut);
        }
//        
        NationDao nationDao=new NationDao(em);
        Nation nation=nationDao.findbyId(1L);
        lstA=auteurDao.findByNation(nation);
        System.out.println(nation);
        for(Auteur aut : lstA) {
            System.out.println(aut);
        }
        
        lstA=auteurDao.getTop5Auteur();
        for(Auteur aut : lstA) {
            System.out.println(aut);
        }
        
        lstA=auteurDao.getTop10AgeAuteur();
        for(Auteur aut : lstA) {
            System.out.println(aut);
        }
        em.close();
        emf.close();
    }
}
