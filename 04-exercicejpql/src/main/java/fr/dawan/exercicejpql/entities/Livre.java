package fr.dawan.exercicejpql.entities;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "livres")
public class Livre extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String titre;

    @Column(nullable = false, name="annee_sortie")
    private int anneeSortie;

    @ManyToOne
    private Categorie categorie;
    
    @ManyToMany(mappedBy="livres")
    private List<Auteur> auteurs=new ArrayList<>();
    
    public Livre() {

    }

    public Livre(String titre, int anneeSortie) {
        this.titre = titre;
        this.anneeSortie = anneeSortie;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getAnneeSortie() {
        return anneeSortie;
    }

    public void setAnneeSortie(int anneeSortie) {
        this.anneeSortie = anneeSortie;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public List<Auteur> getAuteurs() {
        return auteurs;
    }

    public void setAuteurs(List<Auteur> auteurs) {
        this.auteurs = auteurs;
    }

    @Override
    public String toString() {
        return "Livre [" + super.toString() + ", titre=" + titre + ", anneeSortie=" + anneeSortie + "]";
    }

}
