package fr.dawan.exercicejpql.entities;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 100)
    private String nom;
    
    @OneToMany(mappedBy="nation")
    private List<Auteur> auteur=new ArrayList<>();

    public Nation() {

    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public List<Auteur> getAuteur() {
        return auteur;
    }

    public void setAuteur(List<Auteur> auteur) {
        this.auteur = auteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Nation [" + super.toString() + ", nom =" + nom + "]";
    }

}
