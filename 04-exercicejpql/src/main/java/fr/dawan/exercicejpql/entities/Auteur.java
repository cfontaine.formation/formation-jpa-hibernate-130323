package fr.dawan.exercicejpql.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

@Entity
@Table(name = "auteurs")
@NamedQueries({
    @NamedQuery(name = "Auteur.alive",query = "SELECT a FROM Auteur a WHERE a.dateDeces IS NULL"),
    //SELECT a FROM Auteur a LEFT JOIN a.livres l WHERE l IS NULL
    @NamedQuery(name = "Auteur.nobook",query = "SELECT a FROM Auteur a WHERE a.livres IS EMPTY"),
    @NamedQuery(name = "Auteur.byNation",query = "SELECT a FROM Auteur a WHERE a.nation = ?1")
})
public class Auteur extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    @Column(nullable = false, length = 50)
    private String nom;

    @Column(nullable = false, length = 50)
    private String prenom;

    @Column(nullable = false, name = "naissance")
    private LocalDate dateNaissance;

    @Column(name = "deces")
    private LocalDate dateDeces;
    
    @ManyToOne
    private Nation nation;
    
    @ManyToMany
    private List<Livre> livres=new ArrayList<>();

    public Auteur() {

    }

    public Auteur(String nom, String prenom, LocalDate dateNaissance, LocalDate dateDeces) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.dateDeces = dateDeces;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public LocalDate getDateDeces() {
        return dateDeces;
    }

    public void setDateDeces(LocalDate dateDeces) {
        this.dateDeces = dateDeces;
    }
    
    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public List<Livre> getLivres() {
        return livres;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    @Override
    public String toString() {
        return "Auteur [" + super.toString() + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance="
                + dateNaissance + ", dateDeces=" + dateDeces + "]";
    }
}
