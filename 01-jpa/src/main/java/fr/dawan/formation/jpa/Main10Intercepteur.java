package fr.dawan.formation.jpa;

import fr.dawan.formation.jpa.entities.intercepteur.Facture;
import fr.dawan.formation.jpa.entities.intercepteur.Utilisateur;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main10Intercepteur {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Facture fac1 = new Facture(2150.0, 5.5);
        Utilisateur u = new Utilisateur("John", "Doe", "jd@dawan.com", "azerty123");

        try {

            // exemple 1: auditing
            tx.begin();
            em.persist(fac1);
            System.out.println(u);
            em.persist(u);
            System.out.println(u);
            u = em.merge(u);
            u.setPassword("123456");
            System.out.println(u);
            tx.commit();

            // exemple 2
            tx.begin();
            em.clear();
            Facture facRead = em.find(Facture.class, 1L);
            System.out.println(facRead);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }

        em.close();
        emf.close();

    }

}
