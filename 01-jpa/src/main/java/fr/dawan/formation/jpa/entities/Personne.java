package fr.dawan.formation.jpa.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "personnes")
public class Personne extends AbstractEntity {

    private String prenom;

    private String nom;

    public Personne() {
        super();
    }

    public Personne(String prenom, String nom) {
        super();
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Personne [prenom=" + prenom + ", nom=" + nom + "]";
    }
}
