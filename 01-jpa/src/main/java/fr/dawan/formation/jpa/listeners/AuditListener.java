package fr.dawan.formation.jpa.listeners;

import java.time.LocalDateTime;

import fr.dawan.formation.jpa.entities.intercepteur.AbstractAuditable;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

public class AuditListener {

    @PrePersist
    public void onPersist(AbstractAuditable a) {
        System.out.println("@PrePersist");
        a.setCreated(LocalDateTime.now());
    }

    @PreUpdate
    public void onUpdate(AbstractAuditable a) {
        System.out.println("@PreUpdate");
        a.setModified(LocalDateTime.now());
    }

}
