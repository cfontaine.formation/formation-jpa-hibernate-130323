package fr.dawan.formation.jpa.entities.relations;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
@Entity
@Table(name="villes")
public class Ville implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String nom;
    
    // Relation @OneToOne bidirectionnelle 
    // C'est la relation retour  Ville -> Maire
    // pour la réaliser, on place sur la variable d'instance de type Maire
    // on utilise l'annotation @OneToOne avec l'attribut mappedBy qui a pour valeur
    // le nom de variable d'instance de l'autre coté de la relation -> ici ville
    // dans ce cas, on peut connaitre la ville à partir du maire et le maire depuis la ville
    @OneToOne(mappedBy = "ville")
    private Maire maire;
    
    // Pour Relation @OneToOne unidirectionnelle
    // On n'a pas de type Maire dans ville, pour réaliser la relation retour
    // dans ce cas, on peut connaitre la ville à partir du maire, mais pas le maire depuis la ville
    

    public Ville() {
        super();
    }

    public Ville(String nom) {
        super();
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Maire getMaire() {
        return maire;
    }

    public void setMaire(Maire maire) {
        this.maire = maire;
    }

    @Override
    public String toString() {
        return "Ville [id=" + id + ", nom=" + nom + "]";
    }
    
    
}
