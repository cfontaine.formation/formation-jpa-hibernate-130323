package fr.dawan.formation.jpa;

import fr.dawan.formation.jpa.entities.relations.Article;
import fr.dawan.formation.jpa.entities.relations.Fournisseur;
import fr.dawan.formation.jpa.entities.relations.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main08Cascade {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Article a1 = new Article("Tv 4K", 600);
        Article a2 = new Article("Souris gaming", 50);
        Article a3 = new Article("Cable Hdmi", 15);

        Marque mA = new Marque("Marque A");
        Marque mB = new Marque("Marque B");

        a1.setMarque(mA);
        a2.setMarque(mB);
        a3.setMarque(mA);
        mA.getArticles().add(a1);
        mA.getArticles().add(a3);
        mB.getArticles().add(a2);

        Fournisseur f1 = new Fournisseur("Fournisseur 1");
        Fournisseur f2 = new Fournisseur("Fournisseur 2");
        a1.getFournisseurs().add(f1);
        a1.getFournisseurs().add(f2);
        a2.getFournisseurs().add(f1);
        a3.getFournisseurs().add(f2);
        f1.getArticles().add(a1);
        f1.getArticles().add(a2);
        f2.getArticles().add(a1);
        f2.getArticles().add(a3);

        try {
            tx.begin();
            em.persist(a1);
            em.persist(a2);
            em.persist(a3);
            System.out.println("-----------------");
            em.remove(mA);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
