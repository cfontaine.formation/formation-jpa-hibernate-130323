package fr.dawan.formation.jpa;

import java.time.LocalDate;

import org.hibernate.internal.build.AllowSysOut;

import fr.dawan.formation.jpa.entities.Adresse;
import fr.dawan.formation.jpa.entities.Employe;
import fr.dawan.formation.jpa.entities.relations.Article;
import fr.dawan.formation.jpa.entities.relations.Marque;
import fr.dawan.formation.jpa.enums.TypeContrat;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main07StrategieChargement {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Article a1 = new Article("Tv 4K", 600);
        Article a2 = new Article("Souris gaming", 50);
        Article a3 = new Article("Cable Hdmi", 15);

        Marque mA = new Marque("Marque A");
        Marque mB = new Marque("Marque B");

        a1.setMarque(mA);
        a2.setMarque(mB);
        a3.setMarque(mA);
        mA.getArticles().add(a1);
        mA.getArticles().add(a3);
        mB.getArticles().add(a2);

        try {
            tx.begin();
            em.persist(a1);
            em.persist(a2);
            em.persist(a3);
            em.persist(mA);
            em.persist(mB);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.clear(); // retirer les objets de l'entité manager
        System.out.println("-------------------");
//        Article ar1=em.find(Article.class, 1L);
//        System.out.println(ar1.getDescription());
//        System.out.println(ar1.getMarque().getNom()); // Chargement imédiat @ManyToOne ->EAGER
//        System.out.println("-------------------");   
        Marque ma2 = em.find(Marque.class, 2L);
        System.out.println(ma2.getNom()); // Chargement tardif @OneToMany -> Lazy
        System.out.println("-------------------");
//        for(Article a : ma2.getArticles()) {
//            System.out.println(a);
//        }
        em.close();

        for (Article a : ma2.getArticles()) {
            System.out.println(a);
        }
        emf.close();

    }

}
