package fr.dawan.formation.jpa.dao;

import java.util.List;

import fr.dawan.formation.jpa.entities.Personne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class PersonneDao extends AbstractDao<Personne> {

    public PersonneDao(EntityManager em) {
        super(em, Personne.class);
    }

    public List<Personne> findByNom(String nom){
        TypedQuery<Personne> query=em.createQuery("SELECT p FROM Personne p WHERE p.nom = :name",Personne.class );
        query.setParameter("name", nom);
        return query.getResultList();
    }
    
}
