package fr.dawan.formation.jpa.entities.manytomanydata;

import java.io.Serializable;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name = "produit_client")
public class ProduitClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ProduitClientPk id = new ProduitClientPk();

    @ManyToOne
    @MapsId("produitId")
    private Produit produit;

    @ManyToOne
    @MapsId("clientId")
    private Client client;

    private int quantite;

    public ProduitClient() {
    }

    public ProduitClient(Produit produit, Client client, int quantite) {
        this.produit = produit;
        this.client = client;
        this.quantite = quantite;
    }

    public ProduitClientPk getId() {
        return id;
    }

    public void setId(ProduitClientPk id) {
        this.id = id;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

}
