package fr.dawan.formation.jpa.entities.manytomanydata;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;

@Embeddable
public class ProduitClientPk implements Serializable{

    private static final long serialVersionUID = 1L;

    private long produitId;
    
    private long clientId;

    public ProduitClientPk() {

    }

    public ProduitClientPk(long produitId, long clientId) {

        this.produitId = produitId;
        this.clientId = clientId;
    }

    public long getProduitId() {
        return produitId;
    }

    public void setProduitId(long produitId) {
        this.produitId = produitId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, produitId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProduitClientPk other = (ProduitClientPk) obj;
        return clientId == other.clientId && produitId == other.produitId;
    }
    
}
