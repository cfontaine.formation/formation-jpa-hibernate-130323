package fr.dawan.formation.jpa.entities.relations;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "maires")
public class Maire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nom;

    // @OneToOne => Relation 1,1
    // ici, Un maire n'est que d'une seul ville et une ville à seul maire

    // Relation @OneToOne Unidirectionnel
    // on a uniquement une relation Maire -> Ville
    // et pas de relation Ville -> Maire, on n'a pas accés au Maire depuis la Ville
    @OneToOne
   // @JoinColumn(name="id_ville") // @JoinColumn permet de modifier le nom par défaut de la colonne de jointure
    private Ville ville;

    // Relation @OneToOne Bidirectionnel => voir dans la classe Ville
    // on a une relation Maire -> Ville et Ville -> Maire

    public Maire() {
        super();
    }

    public Maire(String nom) {
        super();
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return "Maire [id=" + id + ", nom=" + nom + "]";
    }

}
