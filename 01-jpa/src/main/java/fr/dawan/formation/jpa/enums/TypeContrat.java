package fr.dawan.formation.jpa.enums;

public enum TypeContrat {
    CDI, CDD, INTERIM, STAGE
}
