package fr.dawan.formation.jpa.entities;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.jpa.enums.TypeContrat;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;

// une entité doit :
// - être annoté avec @Entity
// - avoir un attribut qui représente la clé primaire annoté avec @Id
// - implémenté l'interface Serializable
// - avoir obligatoirement un contructeur par défaut

@Entity
//@TableGenerator(name="employegen") // pour strategy = GenerationType.TABLE
//@SequenceGenerator(name="employeseq") // pour strategy = GenerationType.SEQUENCE
@Table(name = "employes") // L'annotation @Table permet de modifier le nom de la table, sinon elle prend le nom de la classe
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    // @Id -> Clé primaire simple
    @Id
    // Génération automatique de la clé primaire
    // @GeneratedValue(strategy = GenerationType.AUTO) // ORM 
    @GeneratedValue(strategy = GenerationType.IDENTITY)   // BDD
    // @GeneratedValue(strategy = GenerationType.TABLE,generator = "employegen") // ORM
    // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "employeseq") // BDD séquence
    private long id;
    
    // Gestion de la concurrence Optimistic (Versioned) => @Version
    @Version    
    private int version; 

    // L'attribut @Column permet pour définir plus précisément la colonne
    @Column(length = 60)    // l'attribut length permet modifier de la longueur d'une chaine de caractère ou d'un @lob 
    private String prenom;  // Sinon par défaut 255 caractères ou octets

    @Column(nullable = false, length = 60) // l'attribut nullable permet de définir si le champ peut être null (optionelle) par défaut true
    private String nom;

    // LocalDate, LocalTime, LocalDateTime sont supportés par hibernate depuis la version 5
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance; 

    private double salaire;
    
    // Une énumération peut être stocké sous forme numérique EnumType.ORDINAL (par
    // défaut)
    // ou sous forme de chaine de caractères EnumType.STRING
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private TypeContrat contrat;

    @Lob // @Lob => pour stocker des données binaires dans la bdd (image, ...) BLOB ou un
         // long texte CLOB
    @Column(length = 70000) // l'attribut length permet de définir la taille du blob => ici un MediumBlob
                            // max 16Mo
                            // pour MYSQL : par défaut un Tinyblob -> 255, Blob-> 65535, MediumBlob -> 16
                            // mo, LongBlob -> 4,29 Go
    private byte[] photoIdentite; // BLOB -> tableau de byte , CLOB -> tableau de caractère

    // Les attributs @Transient ne sont transient ne sont pas persister
    // sinon tous les autres sont par défaut persistant
    @Transient
    private int nePasPersister;
    // ou private transient int nePasPersister;

    // @ Embedded => pour utiliser une classe intégrable
    @Embedded
    private Adresse adressePerso;

    // Pour utiliser plusieurs fois la même classe imbriqué dans la même entitée
    // On aura plusieurs fois le même nom de colonne dans la table => erreur
    // On pourra renommé les colonnes avec des annotations @AttributeOverride placé dans une annotation @AttributeOverrides
    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro")), })
    private Adresse adressePro;
    
    // Mapping d'une collection simple (Integer, String ...)
    @ElementCollection // @ElementCollection -> par défaut, génére une table NomClasse_nomVariable
    // @CollectionTable -> permet de personaliser de la table name: nom de la table, joinColuns -> nom de la colonne de jointure 
    @CollectionTable(name="telephones",joinColumns = @JoinColumn(name="employe_id"))
    private List<String> telephones=new ArrayList<>();
    
    // Constructeur par défaut -> obligatoire avec Hibernate
    public Employe() { 

    }

    public Employe(String prenom, String nom, LocalDate dateNaissance, double salaire, TypeContrat contrat) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.salaire = salaire;
        this.contrat = contrat;
    }

    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public int getNePasPersister() {
        return nePasPersister;
    }

    public void setNePasPersister(int nePasPersister) {
        this.nePasPersister = nePasPersister;
    }

    public byte[] getPhotoIdentite() {
        return photoIdentite;
    }

    public void setPhotoIdentite(byte[] photoIdentite) {
        this.photoIdentite = photoIdentite;
    }

    public TypeContrat getContrat() {
        return contrat;
    }

    public void setContrat(TypeContrat contrat) {
        this.contrat = contrat;
    }

    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }
    

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }

    public List<String> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<String> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        return "Employe [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance
                + ", salaire=" + salaire + ", contrat=" + contrat + ", adressePerso=" + adressePerso + ", adressePro="
                + adressePro + "]";
    }

}
