package fr.dawan.formation.jpa.entities.intercepteur;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PostLoad;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;

@Entity
@Table(name = "factures")
public class Facture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    private double montantHt;

    private double tva;

    @Transient
    private double montantTtc;

    public Facture() {

    }

    public Facture(double montantHt, double tva) {
        super();
        this.montantHt = montantHt;
        this.tva = tva;
        montantTtc = montantHt * (tva / 100.0);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getMontantHt() {
        return montantHt;
    }

    public void setMontantHt(double montantHt) {
        this.montantHt = montantHt;
    }

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public double getMontantTtc() {
        return montantTtc;
    }

    public void setMontantTtc(double montantTtc) {
        this.montantTtc = montantTtc;
    }

    @PreUpdate
    public void onPreUpdate() {
        System.out.println("Preupdate");
    }

    @PrePersist
    public void onPrePersist() {
        System.out.println("Pre persit");
    }

    @PostLoad
    public void onPostLoad() {
        System.out.println("Post Load");
        montantTtc = montantHt * (tva / 100.0);
    }

    @Override
    public String toString() {
        return "Facture [id=" + id + ", montantHt=" + montantHt + ", tva=" + tva + ", montantTtc=" + montantTtc + "]";
    }
    
    
}
