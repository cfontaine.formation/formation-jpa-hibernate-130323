package fr.dawan.formation.jpa.dao;

import java.util.List;

import fr.dawan.formation.jpa.entities.AbstractEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;

public abstract class AbstractDao<T extends AbstractEntity> {
    
    protected EntityManager em;
    
    private Class<T> classEntity;
    
    public AbstractDao(EntityManager em, Class<T> classEntity) {
        this.em = em;
        this.classEntity = classEntity;
    }

    public void saveOrUpdate(T entity) throws Exception {
        EntityTransaction tx=em.getTransaction();
        try {
            tx.begin();
            if(entity.getId()==0) {
                em.persist(entity);
            }
            else {
                em.merge(entity);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
        
    }
    
    public void remove(T entity) throws Exception{
        EntityTransaction tx=em.getTransaction();
        
        try {
            tx.begin();
            em.remove(entity);
            tx.commit();
        } catch (Exception e) {
           tx.rollback();
           throw e;
        }
    }
    
    public void remove(long id)  throws Exception {
       remove(em.merge(findById(id)));
    }
    
    public T findById(long id) {
        return em.find(classEntity, id);
    }
    
    public List<T> findAll(){
        TypedQuery<T> query=em.createQuery("SELECT e FROM "+classEntity.getName() + " e",classEntity);
        return query.getResultList();
    }
    

}
