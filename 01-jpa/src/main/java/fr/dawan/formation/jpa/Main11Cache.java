package fr.dawan.formation.jpa;

import fr.dawan.formation.jpa.entities.Personne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main11Cache {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        Personne p1 = new Personne("John", "Doe");
        testCacheNiveau1(em, p1);
        em.close();
        emf.close();
    }

    public static void testCacheNiveau1(EntityManager em, Personne p) {
        EntityTransaction tx = em.getTransaction();
        // Cache de niveau I => il se trouve dans l'entity manager
        // but: amélioration des performences en réduisant le nombre de requete vers la
        // base de données
        try {
            tx.begin();
            em.persist(p);
            long id = p.getId();
            System.out.println(p);
            em.detach(p); // -> l'entité n'est plus gérer par l'entity manager

            // comme l'entité n'est plus géré par l'entity manager, une requete SQL select
            // est exécutée par hibernate
            Personne p2 = em.find(Personne.class, id);
            System.out.println(p2);
            // comme il est suivie par l'entity manager,
            // aucune requète SQL n'est éxécutée, l'entité est récupéré en mémoire dans la
            // cache de niveau 1
            Personne p3 = em.find(Personne.class, id);
            System.out.println(p3);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();

        }
    }
}
