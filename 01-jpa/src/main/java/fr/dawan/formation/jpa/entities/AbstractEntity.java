package fr.dawan.formation.jpa.entities;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;

// @MappedSuperclass -> Annotation placé sur la classe mère
// Aucune table ne correspondra à la classe mère dans la base de données
// L’état de la classe mère sera rendu persistant dans les tables associées à ses classes entités filles
@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
