package fr.dawan.formation.jpa;

import java.time.LocalDate;

import fr.dawan.formation.jpa.entities.Adresse;
import fr.dawan.formation.jpa.entities.Employe;
import fr.dawan.formation.jpa.enums.TypeContrat;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main06CycleDeVie {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Employe emp1 = new Employe("John", "Doe", LocalDate.of(1989, 10, 12), 1950, TypeContrat.CDI);
        Adresse adrPerso = new Adresse("1, rue Esquermoise", "Lille", "59800");
        Adresse adrPro = new Adresse("46,rue des cannoniers", "Lille", "59800");
        emp1.setAdressePerso(adrPerso);
        emp1.setAdressePro(adrPro);

        System.out.println(emp1);
        try {
            tx.begin();
            em.persist(emp1);
//            System.out.println("persist -------------");
//            em.detach(emp1);
//            System.out.println("detach -------------");
//            emp1=em.merge(emp1);
//            System.out.println("merge ------------");
//            em.remove(emp1);
//            System.out.println("remove -----------------");
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.clear();
        emp1 = em.find(Employe.class, 1L);
        System.out.println(emp1);
        emp1 = em.find(Employe.class, 1L);
        System.out.println(emp1);

        em.close();

//        EntityManager em2 = emf.createEntityManager();
//        tx=em2.getTransaction();
//        try {
//            tx.begin();
//            emp1=em2.merge(emp1);
//            emp1.setSalaire(1960);
//            em2.remove(emp1);
//            tx.commit();
//        } catch (Exception e) {
//            tx.rollback();
//            e.printStackTrace();
//        }
//        System.out.println(emp1);
//        
//        em2.close();
        emf.close();

    }

}
