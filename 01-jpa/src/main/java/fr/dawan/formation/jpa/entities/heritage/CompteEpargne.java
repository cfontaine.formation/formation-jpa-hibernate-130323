package fr.dawan.formation.jpa.entities.heritage;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
// pour SINGLE_TABLE
//Pour la classe CompteEpargne la valeur dans la colonne discriminator => CE
@DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    private double taux=5.0;

    public CompteEpargne() {
        super();
    }

    public CompteEpargne(String titulaire, double solde, String iban, double taux) {
        super(titulaire, solde, iban);

    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }

}
