package fr.dawan.formation.jpa;

import fr.dawan.formation.jpa.entities.manytomanydata.Client;
import fr.dawan.formation.jpa.entities.manytomanydata.Produit;
import fr.dawan.formation.jpa.entities.manytomanydata.ProduitClient;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main05ManyToManyData {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Produit pr1 = new Produit("TV 4K", 650.0);
        Produit pr2 = new Produit("Stylo bille", 1.5);

        Client cl1 = new Client("John", "Doe");
        Client cl2 = new Client("Jane", "Doe");
        Client cl3 = new Client("Alan", "Smithee");

        ProduitClient pc1 = new ProduitClient(pr1, cl1, 5);
        ProduitClient pc2 = new ProduitClient(pr2, cl1, 3);
        ProduitClient pc3 = new ProduitClient(pr1, cl2, 7);
        ProduitClient pc4 = new ProduitClient(pr1, cl3, 6);
        ProduitClient pc5 = new ProduitClient(pr2, cl3, 2);

        try {
            tx.begin();
            em.persist(pr1);
            em.persist(pr2);
            em.persist(cl1);
            em.persist(cl2);
            em.persist(cl3);
            em.persist(pc1);
            em.persist(pc2);
            em.persist(pc3);
            em.persist(pc4);
            em.persist(pc5);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
