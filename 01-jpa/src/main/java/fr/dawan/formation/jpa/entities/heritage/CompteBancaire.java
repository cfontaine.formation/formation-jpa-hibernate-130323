package fr.dawan.formation.jpa.entities.heritage;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;

@Entity

// Héritage => trois façons d’organiser l’héritage

// 1 - SINGLE_TABLE => Le parent et les enfants vont être placé dans la même table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// Une colonne "Discriminator" définit le type de la classe enregistrée
@DiscriminatorColumn(name = "type_compte")
// Pour la classe CompteBancaire la valeur dans la colonne discriminator => CB
@DiscriminatorValue("CB")

// 2 - TABLE_PER_CLASS => le parent et chaque enfant auront leur propre table
// la table enfant aura les colonnes des variables d'instance de la classe parent
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

// 3 - JOINED => on aura une jointure entre la table de la classe parent et la table de la classe enfant
//@Inheritance(strategy = InheritanceType.JOINED)
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(length=100)
    private String titulaire;

    private double solde;

    @Column(length=34)
    private String iban;

    public CompteBancaire() {
        super();
    }

    public CompteBancaire(String titulaire, double solde, String iban) {
        super();
        this.titulaire = titulaire;
        this.solde = solde;
        this.iban = iban;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public String toString() {
        return "CompteBancaire [id=" + id + ", titulaire=" + titulaire + ", solde=" + solde + ", iban=" + iban + "]";
    }
}
