package fr.dawan.formation.jpa;

import java.time.LocalDate;

import fr.dawan.formation.jpa.entities.Adresse;
import fr.dawan.formation.jpa.entities.Employe;
import fr.dawan.formation.jpa.enums.TypeContrat;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main01Entite {
    public static void main(String[] args) {
        // Création de l'entity Manager Factory
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        // Création de l'entity Manager
        EntityManager em = emf.createEntityManager();
        // Création de la transaction
        EntityTransaction tx = em.getTransaction();

        Employe emp1 = new Employe("John", "Doe", LocalDate.of(1998, 7, 13), 2100, TypeContrat.CDI);
        Adresse adrPerso = new Adresse("1,rue esquemoise", "59800", "Lille");
        Adresse adrPro = new Adresse("46, rue des cannonier", "59800", "Lille");
        emp1.setAdressePerso(adrPerso);
        emp1.setAdressePro(adrPro);

        System.out.println(emp1);
        
        try {
            tx.begin(); // -> démarrer la transaction
            // Peristence de l'objet p1 dans la bdd
            em.persist(emp1);
            // emp1 est géré par le entité manager -> quand on modifie l'état de emp1, elle est persitée dans la bdd
            emp1.setSalaire(2200);
            // Suppression de l'objet emp1 dans la bdd
            em.remove(emp1);
            tx.commit(); // transaction -> valider
        } catch (Exception e) {
            tx.rollback(); // transaction -> annuler
        }
        em.close();     // fermeture de l'entity manager
        emf.close();    // fermeture de l'entity manager factory

    }
}
