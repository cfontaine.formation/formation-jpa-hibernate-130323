package fr.dawan.formation.jpa;

import java.time.LocalDate;

import fr.dawan.formation.jpa.entities.Employe;
import fr.dawan.formation.jpa.entities.heritage.CompteBancaire;
import fr.dawan.formation.jpa.entities.heritage.CompteEpargne;
import fr.dawan.formation.jpa.enums.TypeContrat;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main02Heritage {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        CompteBancaire cb1=new CompteBancaire("John Doe", 560.0, "az-1235");
        CompteBancaire cb2=new CompteBancaire("Jane Doe", 1560.0, "de-4567");
        CompteEpargne ce1=new CompteEpargne("John Doe", 560.0, "az-1237",4);
        CompteEpargne ce2=new CompteEpargne("John Doe", 1560.0, "de-4567",4);
        
        Employe emp1=new Employe("John","Doe",LocalDate.of(1993,6,4),1900,TypeContrat.CDI);
        emp1.getTelephones().add("0320000000");
        emp1.getTelephones().add("0600000000");
        
        Employe emp2=new Employe("Jane","Doe",LocalDate.of(1990,6,4),2300,TypeContrat.CDI);
        emp2.getTelephones().add("0327000000");
        try {
            tx.begin();

            em.persist(cb1);
            em.persist(cb2);
            em.persist(ce1);
            em.persist(ce2);
            
            em.persist(emp1);
            em.persist(emp2);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
