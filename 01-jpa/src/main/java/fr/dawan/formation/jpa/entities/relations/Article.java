package fr.dawan.formation.jpa.entities.relations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "articles")
public class Article implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;

    private double prix;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="id_marque")
    private Marque marque;
    
    @ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
//    @JoinTable(name="article2fournisseur",
//    joinColumns = @JoinColumn(name="fk_article"),
//    inverseJoinColumns = @JoinColumn(name="fk_fournisseur"))
    private List<Fournisseur> fournisseurs=new ArrayList<>();
    
    public Article() {
        super();
    }

    public Article(String description, double prix) {
        super();
        this.description = description;
        this.prix = prix;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", description=" + description + ", prix=" + prix + "]";
    }

}
