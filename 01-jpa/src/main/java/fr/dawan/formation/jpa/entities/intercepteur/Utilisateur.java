package fr.dawan.formation.jpa.entities.intercepteur;

import fr.dawan.formation.jpa.listeners.AuditListener;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;

@Entity
@EntityListeners(AuditListener.class)
public class Utilisateur extends AbstractAuditable{
    
    private String prenom;

    private String nom;
    
    private String login;
    
    private String password;

    public Utilisateur() {
        super();
    }

    public Utilisateur(String prenom, String nom, String login, String password) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.login = login;
        this.password = password;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Utilisateur [prenom=" + prenom + ", nom=" + nom + ", login=" + login + ", password=" + password
                + ", toString()=" + super.toString() + "]";
    }
}
