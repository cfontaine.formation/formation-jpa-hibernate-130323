package fr.dawan.formation.jpa;

import java.util.List;

import fr.dawan.formation.jpa.dao.PersonneDao;
import fr.dawan.formation.jpa.entities.Personne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class Main09Dao {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        PersonneDao dao = new PersonneDao(em);
        Personne p1 = new Personne("John", "Doe");
        Personne p2 = new Personne("Jane", "Doe");
        Personne p3 = new Personne("Alan", "Smithee");
        Personne p4 = new Personne("Yves", "Rouleau");

        try {
            dao.saveOrUpdate(p1);
            dao.saveOrUpdate(p2);
            dao.saveOrUpdate(p3);
            dao.saveOrUpdate(p4);

            List<Personne> lst = dao.findAll();
            for (Personne p : lst) {
                System.out.println(p);
            }

            p1.setPrenom("Marcel");
            dao.saveOrUpdate(p1);

            Personne per1 = dao.findById(2L);
            System.out.println(per1);

            dao.remove(p4);

            dao.remove(2L);

            lst = dao.findAll();
            for (Personne p : lst) {
                System.out.println(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        em.close();
        emf.close();

    }

}
