package fr.dawan.formation.jpa;

import fr.dawan.formation.jpa.entities.relations.Maire;
import fr.dawan.formation.jpa.entities.relations.Ville;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main03OneToOne {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
       Ville v1=new Ville("Lille");
       Ville v2=new Ville("Marseille");
       Ville v3=new Ville("Marseille");
       
       Maire m1=new Maire("Martine");
       Maire m2=new Maire("Anne");
       Maire m3=new Maire("Jean-luc");
       
       m1.setVille(v1);
       v1.setMaire(m1); 
        try {
            tx.begin();
            em.persist(v1);
            em.persist(v2);
            em.persist(v3);
            em.persist(m1);
            em.persist(m2);
            em.persist(m3);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
