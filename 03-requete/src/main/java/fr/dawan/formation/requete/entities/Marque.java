package fr.dawan.formation.requete.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedNativeQueries;
import jakarta.persistence.NamedNativeQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "marques")
@NamedNativeQueries({
    @NamedNativeQuery(  name = "Marque.findbynom",
                        query = "SELECT m.id, m.version, m.nom FROM marques m WHERE nom=:nom",
                        resultClass = Marque.class)
   // , @NamedNativeQuery(  ) // ...
})
public class Marque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false, length = 50)
    private String nom;

    @OneToMany(mappedBy = "marque")
    private List<Article> articles = new ArrayList<>();

    public Marque() {

    }

    public Marque(String nom) {
        this.nom = nom;
    }

    public List<Article> getArticle() {
        return articles;
    }

    public void setArticle(List<Article> articles) {
        this.articles = articles;
    }

    public void ajoutArticle(Article article) {
        if (!articles.contains(article)) {
            articles.add(article);
            article.setMarque(this);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Marque [id=" + id + ", version=" + version + ", nom=" + nom + "]";
    }

}
