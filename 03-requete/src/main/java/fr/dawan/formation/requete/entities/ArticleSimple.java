package fr.dawan.formation.requete.entities;

public class ArticleSimple {

    private String description;

    private double prix;

    public ArticleSimple(String description, double prix) {
        this.description = description;
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "ArticleSimple [description=" + description + ", prix=" + prix + "]";
    }

}
