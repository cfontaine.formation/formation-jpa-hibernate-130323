package fr.dawan.formation.requete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import fr.dawan.formation.requete.enums.Emballage;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "articles")

// @NamedQueries, @NamedQuery
// Les définitions de requête nommées se placent uniquement dans les entités
// Le nom de la requête nommée doit être unique parmi toutes les entités de l’unité de persistance
// On préfixera le nom de la requête avec le nom de l'entité pour éviiter les colisionde nom
// name -> nom de la requete nommée
// query -> requete JPQLS
@NamedQueries(value = { 
        @NamedQuery(name = "Article.prixLess", query = "SELECT a FROM Article a WHERE a.prix<?1"),
        @NamedQuery(name = "Article.prixMore", query = "SELECT a FROM Article a WHERE a.prix>?1") 
        })

public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    private String description;

    private double prix;

    @Enumerated(EnumType.STRING)
    private Emballage emballage;

    @ManyToOne
    @JoinColumn(name = "id_marque")
    private Marque marque;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "article2fournisseur", joinColumns = @JoinColumn(name = "fk_article"), inverseJoinColumns = @JoinColumn(name = "fk_founisseur"))
    private Set<Fournisseur> fournisseurs = new HashSet<>();

    public Article() {

    }

    public Article(String description, double prix, Emballage emballage) {
        super();
        this.description = description;
        this.prix = prix;
        this.emballage = emballage;
    }

    public Set<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(Set<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Emballage getEmballage() {
        return emballage;
    }

    public void setEmballage(Emballage emballage) {
        this.emballage = emballage;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", description=" + description + ", prix=" + prix + ", emballage=" + emballage
                + "]";
    }

}
