package fr.dawan.formation.requete;

import java.util.List;

import fr.dawan.formation.requete.entities.Article;
import fr.dawan.formation.requete.entities.ArticleSimple;
import fr.dawan.formation.requete.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;

public class Main01JPQL {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        // En SQL: SELECT * FROM articles
        // En JPQL: SELECT a FROM Article as a
        
        // createQuery -> permet de créer les requêtes JPQL depuis l’Entity Manager
        // getSingleResult -> pour récupérer un résultat unique, s'il n'y a pas de résulat ou plusieurs => exception
        // getResultList() -> pour récupérer un résultat multiple (List)
        
        // Query et TypedQuery: représente la requête
        // Query
        Query query1 = em.createQuery("SELECT a FROM Article as a"); // as est optionnel
        List<Article> ar = (List<Article>) query1.getResultList();
        for (Article a : ar) {
            System.out.println(a);
        }

        // TypedQuery -> à privilégier par rapport à Query => Type Générique
        TypedQuery<Article> query2 = em.createQuery("SELECT a FROM Article as a", Article.class);
        List<Article> ar2 = query2.getResultList();
        for (Article a : ar2) {
            System.out.println(a);
        }
        
        // createQuery -> permet de créer les requêtes JPQL depuis l’Entity Manager
        // getSingleResult -> pour récupérer un résultat unique, s'il n'y a pas de résulat ou plusieurs => exception
        // getResultList() -> pour récupérer un résultat multiple (List)

        // WHERE
        TypedQuery<Article> query3 = em.createQuery("SELECT a FROM Article a WHERE a.prix<30", Article.class);
        for (Article a : query3.getResultList()) {
            System.out.println(a);
        }

        // new
        TypedQuery<ArticleSimple> query4 = em.createQuery(
                "SELECT new fr.dawan.formation.requete.entities.ArticleSimple(a.description,a.prix) FROM Article a",
                ArticleSimple.class);
        List<ArticleSimple> ar3 = query4.getResultList();
        for (ArticleSimple as : ar3) {
            System.out.println(as);
        }

        // Paramètre de requête

        // paramètre de position -> ?numero
        TypedQuery<Article> query5 = em.createQuery("SELECT a FROM Article a WHERE a.prix<?1 AND a.prix>?2",
                Article.class);
        query5.setParameter(1, 50.0); // => setParameter, pour définir la valeur des paramètres
        query5.setParameter(2, 10.0);
        List<Article> ar5 = query5.getResultList();
        for (Article a : ar5) {
            System.out.println(a);
        }

        // paramètre nommés -> :nomParametre
        TypedQuery<Article> query6 = em
                .createQuery("SELECT a FROM Article a WHERE a.prix< :prixmax AND a.prix> :prixmin", Article.class);
        query6.setParameter("prixmax", 50.0);
        query6.setParameter("prixmin", 10.0);
        List<Article> ar6 = query6.getResultList();
        for (Article a : ar6) {
            System.out.println(a);
        }

        // BETWEEN -> définir un interval
        TypedQuery<Article> query7 = em
                .createQuery("SELECT a FROM Article a WHERE a.prix BETWEEN :prixmin AND :prixmax", Article.class);
        query7.setParameter("prixmax", 50.0);
        query7.setParameter("prixmin", 10.0);
        List<Article> ar7 = query7.getResultList();
        for (Article a : ar7) {
            System.out.println(a);
        }

        // IN -> définir un ensemble de valeur
        TypedQuery<Article> query8 = em.createQuery("SELECT a FROM Article a WHERE a.prix IN(70.0,40.0,350.0)",
                Article.class);
        List<Article> ar8 = query8.getResultList();
        for (Article a : ar8) {
            System.out.println(a);
        }

        List<Article> lst;

        // LIKE -> recherche sur un modèle particulier
        // Jokers: % -> 0,1 ou n caractères, – _ -> 1 caractère
        // Ici, on recherche tous les articles dont la description commence par M et
        // fait au moins 4 caractères
        lst = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE 'M____%'", Article.class)
                .getResultList();
        for (Article a : lst) {
            System.out.println(a);
        }

        // ESCAPE => pour utiliser % ou _ en temps que caractère pour qu'ils ne soient
        // pas interprété comme un Joker
        lst = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE '%100@%%' ESCAPE '@'", Article.class)
                .getResultList();
        for (Article a : lst) {
            System.out.println(a);
        }

        // IS NULL -> pour tester si un variable d'instance est null
        lst = em.createQuery("SELECT a FROM Article a WHERE a.marque IS NULL", Article.class).getResultList();
        for (Article a : lst) {
            System.out.println(a);
        }

        // IS EMPTY -> permet de tester, si une colection est vide
        List<Marque> lstm = em.createQuery("SELECT m FROM Marque m WHERE m.articles IS EMPTY", Marque.class)
                .getResultList();
        for (Marque a : lstm) {
            System.out.println(a);
        }

        // Expression de chemin

        // Pour un @OneToOne, @ManyToONE
        // marque est une "simple" variable d'instanse, on peut accéder au nom
        lst = em.createQuery("SELECT a FROM Article a WHERE a.marque.nom = 'Marque B'", Article.class).getResultList();
        for (Article a : lst) {
            System.out.println(a);
        }

        // Pour un @OneToMany, @ManyToMany
        // ERREUR articles est une collection => il faut faire une jointure
        // List<Marque> lstm=em.createQuery("SELECT m FROM Marque m WHERE
        // m.articles.emballage = 'SANS'",Marque.class).getResultList();
        // for(Marque a : lstm) {
        // System.out.println(a);
        // }

        // Fonctions d’agrégations
        // COUNT -> compte le nombre d’élément
        long nbArticle = em.createQuery("SELECT COUNT(a) FROM Article as a WHERE a.prix>50.0", Long.class)
                .getSingleResult();
        System.out.println(nbArticle);

        // AVG -> Moyenne
        double moyPrix = em.createQuery("SELECT AVG(a.prix) FROM Article as a WHERE a.prix>50.0", Double.class)
                .getSingleResult();
        System.out.println(moyPrix);

        // Fonction Chaine de caractères
        // LOCATE -> recherche d’une sous-chaîne et retourne la position
        List<Integer> positions = em.createQuery("SELECT Locate('DDR',a.description) FROM Article a", Integer.class)
                .getResultList();
        for (int i : positions) {
            System.out.println(i);
        }

        // CONCAT -> concaténation de chaine de caractères
        List<String> lstStr = em
                .createQuery("SELECT CONCAT(UPPER(m.nom),':',SIZE(m.articles)) FROM Marque m", String.class)
                .getResultList();
        for (String s : lstStr) {
            System.out.println(s);
        }

        // ORDER BY -> trier, par défaut le tri est croissant, DESC pour qu'il soit
        // décroissant
        // Il peut y avoir plusieurs critères de tri
        lst = em.createQuery("SELECT a FROM Article a ORDER BY a.prix DESC,a.description", Article.class)
                .getResultList();
        for (Article a : lst) {
            System.out.println(a);
        }

        // GROUP BY -> pour grouper plusieurs résultats, On peut regrouper sur un alias
        // ou sur attribut
        List<Long> nbArticleBymarquee = em
                .createQuery("SELECT Count(a.marque) FROM Article a GROUP BY a.marque HAVING Count(a.marque)>3",
                        Long.class)
                .getResultList();
        for (long a : nbArticleBymarquee) {
            System.out.println(a);
        }

        List<String> nbArticleByEmballage = em
                .createQuery("SELECT CONCAT( a.emballage,' ' ,Count(a.emballage)) FROM Article a GROUP BY a.emballage",
                        String.class)
                .getResultList();
        for (String a : nbArticleByEmballage) {
            System.out.println(a);
        }

        // JOIN -> Jointure Interne
        List<String> lstJoin;
        lstJoin = em.createQuery("SELECT CONCAT(m.nom, ' ',a.description ,' ',a.prix)  FROM Marque m JOIN m.articles a",
                String.class).getResultList();
        for (String s : lstJoin) {

            System.out.println(s);

        }

        lstJoin = em.createQuery(
                "SELECT CONCAT(f.nom, ' ',a.description ,' ',a.prix)  FROM Article a JOIN a.fournisseurs f where f.nom='Fournisseur 2'",
                String.class).getResultList();
        for (String s : lstJoin) {

            System.out.println(s);

        }

        // LEFT JOIN -> jointure externe
        lstJoin = em
                .createQuery("SELECT CONCAT(m.nom, ':',COUNT(a)) FROM Marque m LEFT JOIN m.articles a GROUP BY m.nom",
                        String.class)
                .getResultList();
        for (String s : lstJoin) {

            System.out.println(s);

        }

        // JOIN FETCH -> pour récupérer les entités associées en une seule requête
        //               au lieu de requêtes supplémentaires pour chaque accès aux relations lazy
        List<Marque> lstMarque = em.createQuery("SELECT m FROM Marque m JOIN FETCH m.articles ", Marque.class)
                .getResultList();
        System.out.println("----------------");
        em.close();
        System.out.println(lstMarque.get(0).getArticle().get(0).getDescription());

        // Sous-requête
        List<String> lstAr10 = em.createQuery(
                "SELECT CONCAT(ar.description, ' ', ar.marque.nom) FROM Article ar WHERE (SELECT count(a) FROM Article a WHERE ar.marque=a.marque) >3",
                String.class).getResultList();
        for (String s : lstAr10) {
            System.out.println(s);
        }

        // Requètes nommées =>  amélioration des performances (pré compilées par le
        //                      fournisseur de persistance au démarrage de l’application)
        // createNamedQuery -> permet de les utiliser avec l'entity manager
        List<Article> articles = em.createNamedQuery("Article.prixLess", Article.class).setParameter(1, 100)
                .getResultList();
        for (Article a : articles) {
            System.out.println(a);
        }

        articles = em.createNamedQuery("Article.prixMore", Article.class).setParameter(1, 100).getResultList();
        for (Article a : articles) {
            System.out.println(a);
        }

        // setMaxResults permet de limiter le résultat => équivalant à LIMIT en SQL
        articles = em.createQuery("SELECT a FROM Article a ORDER BY a.prix DESC", Article.class).setMaxResults(3)
                .getResultList();
        for (Article a : articles) {
            System.out.println(a);
        }

        // UPDATE =>    pour mettre à jour à un ensemble d'entité
        //              Doit obligatoirement se trouver dans une transaction
        //              executeUpdate permet d'exécuter la requête dans l'entity manager
        int nbLigne = 0;
        try {
            em.getTransaction().begin();
            nbLigne = em.createQuery("UPDATE Article a SET a.prix=a.prix*1.10 WHERE a.prix>:prixmin")
                    .setParameter("prixmin", 50.0).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        System.out.println(nbLigne); // retourne le nombre d'entité mis à jour

        // DELETE =>    pour supprimer un ensemble d'entité
        //              Doit obligatoirement se trouver dans une transaction
        //              executeUpdate pour exécuter la requête
        try {
            em.getTransaction().begin();
            nbLigne = em.createQuery("DELETE FROM Article a WHERE a.marque IS NULL").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        System.out.println(nbLigne); // retourne le nombre d'entité supprimer
        
        em.close();
        emf.close();
    }
}
