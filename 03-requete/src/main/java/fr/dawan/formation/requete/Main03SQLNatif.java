package fr.dawan.formation.requete;

import java.util.List;

import fr.dawan.formation.requete.entities.Article;
import fr.dawan.formation.requete.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class Main03SQLNatif {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        List<Article> lst = em.createNativeQuery(
                "SELECT * FROM articles a WHERE a.prix<:prix LIMIT 2",
                Article.class).setParameter("prix", 100.0).getResultList();
        
        for (Article result : lst) {
            System.out.println(result);
        }

        List<Marque> lstMarque = em.createNamedQuery("Marque.findbynom", Marque.class)
                .setParameter("nom", "Marque B").getResultList();
        for (Marque result : lstMarque) {
            System.out.println(result);
        }
        em.close();
        emf.close();
    }

}
