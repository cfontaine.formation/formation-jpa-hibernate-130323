package fr.dawan.formation.requete;

import java.util.List;

import fr.dawan.formation.requete.entities.Article;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

public class Main02Criteria {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Article> cQuery = cBuilder.createQuery(Article.class);
        // SELECT a FROM ARTICLE a WHERE a.prix<100.0 ORDER BY a.prix
        Root<Article> ar = cQuery.from(Article.class);
        cQuery.where(cBuilder.lessThan(ar.get("prix"), 100.0)).select(cQuery.getSelection())
                .orderBy(cBuilder.asc(ar.get("prix")));

        TypedQuery<Article> query = em.createQuery(cQuery);
        List<Article> lstArticles = query.getResultList();
        lstArticles.forEach(System.out::println);

        em.close();
        emf.close();
    }
}
