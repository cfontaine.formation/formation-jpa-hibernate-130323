package fr.dawan.exercicejpa;

import fr.dawan.exercicejpa.entities.pizzeria.Ingredient;
import fr.dawan.exercicejpa.entities.pizzeria.Pizza;
import fr.dawan.exercicejpa.enums.Base;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class App 
{
    public static void main( String[] args )
    {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("exercicejpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        Ingredient mozza=new Ingredient("Mozzarella");
        Pizza margherita=new Pizza("Margherita", Base.ROUGE,11.0,null);
        Pizza pSaumon=new Pizza("Pizza  au Saumon", Base.BLANCHE,16.5,null);
        
        try {
            tx.begin();
            em.persist(mozza);
            em.persist(margherita);
            em.persist(pSaumon);
            
            margherita.setPrix(margherita.getPrix()*1.1);
            pSaumon.setPrix(pSaumon.getPrix()*1.1);
            
            System.out.println(margherita);
            System.out.println(pSaumon);
            
            em.remove(mozza);
            
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }

        em.close();
        emf.close();
    }
}
