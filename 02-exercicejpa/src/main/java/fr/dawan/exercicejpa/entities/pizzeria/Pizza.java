package fr.dawan.exercicejpa.entities.pizzeria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.exercicejpa.enums.Base;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "pizzas")
public class Pizza implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @ManyToMany
    private List<Ingredient> ingredients=new ArrayList<>();

    @Column(length = 40, nullable = false)
    private String nom;

    @Enumerated(EnumType.STRING)
    @Column(length = 8,nullable = false)
    private Base base;

    @Column(nullable = false)
    private double prix;

    @Lob
    @Column(length = 70000)
    private byte[] photo;
    
    public Pizza() {

    }

    public Pizza(String nom, Base base, double prix, byte[] photo) {

        this.nom = nom;
        this.base = base;
        this.prix = prix;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", nom=" + nom + ", base=" + base + ", prix=" + prix 
               + "]";
    }

}
