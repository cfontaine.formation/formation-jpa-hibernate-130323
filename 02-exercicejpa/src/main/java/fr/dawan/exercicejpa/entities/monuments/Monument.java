package fr.dawan.exercicejpa.entities.monuments;


import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="monuments")
public class Monument extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    @Column(nullable = false)
    private String nom;
    
    @Column(nullable = false)
    private int anneeConstruction;

    
    @OneToOne
    private Coordonee coordonee;
    
    @ManyToOne
    private Nation nation;
    
    @ManyToMany
    private List<Tag> tags=new ArrayList<>();
    
    public Monument() {
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAnneeConstruction() {
        return anneeConstruction;
    }

    public void setAnneeConstruction(int anneeConstruction) {
        this.anneeConstruction = anneeConstruction;
    }

    public Coordonee getCoordonee() {
        return coordonee;
    }


    public void setCoordonee(Coordonee coordonee) {
        this.coordonee = coordonee;
    }


    public Nation getNation() {
        return nation;
    }


    public void setNation(Nation nation) {
        this.nation = nation;
    }


    public List<Tag> getTags() {
        return tags;
    }


    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }


    @Override
    public String toString() {
        return "Monument [nom=" + nom + ", anneeConstruction=" + anneeConstruction + "]";
    }
    
}

