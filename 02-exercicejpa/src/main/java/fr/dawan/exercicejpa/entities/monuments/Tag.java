package fr.dawan.exercicejpa.entities.monuments;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="tags")
public class Tag extends AbstractEntity {
    
    private static final long serialVersionUID = 1L;
    
    @Column(nullable = false)
    private String nom;

    @ManyToMany(mappedBy="tags")
    private List<Monument> monuments=new ArrayList<>(); 
    
    public Tag() {
    }

    public Tag(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Monument> getMonuments() {
        return monuments;
    }

    public void setMonuments(List<Monument> monuments) {
        this.monuments = monuments;
    }

    @Override
    public String toString() {
        return "Tag [nom=" + nom + "]";
    }
}
