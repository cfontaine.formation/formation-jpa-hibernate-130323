package fr.dawan.exercicejpa.entities.qcm;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="questions")
public class QcmQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String qstText;

    private boolean multiple;

    private int numOrder;

    @ManyToOne
    private Qcm qcm;
    
    public QcmQuestion() {
    }

    public QcmQuestion(String qstText, boolean multiple, int numOrder) {
        this.qstText = qstText;
        this.multiple = multiple;
        this.numOrder = numOrder;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQstText() {
        return qstText;
    }

    public void setQstText(String qstText) {
        this.qstText = qstText;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public int getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(int numOrder) {
        this.numOrder = numOrder;
    }

    public Qcm getQcm() {
        return qcm;
    }

    public void setQcm(Qcm qcm) {
        this.qcm = qcm;
    }

    @Override
    public String toString() {
        return "QcmQuestion [id=" + id + ", qstText=" + qstText + ", multiple=" + multiple + ", numOrder=" + numOrder
                + "]";
    }

}

