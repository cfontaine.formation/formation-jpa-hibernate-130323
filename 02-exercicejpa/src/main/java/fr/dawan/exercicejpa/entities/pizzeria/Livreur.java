package fr.dawan.exercicejpa.entities.pizzeria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "livreurs")
public class Livreur implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy="livreur")
    private List<Commande> commandes=new ArrayList<>();
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;
    
    @Column(length = 60, nullable = false)
    private String nom;

    @Column(length = 15, nullable = false)
    private String telephone;

    public Livreur() {
    }

    public Livreur(String nom, String telephone) {
        this.nom = nom;
        this.telephone = telephone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    @Override
    public String toString() {
        return "Livreur [id=" + id + ", nom=" + nom + ", telephone=" + telephone + "]";
    }

}
