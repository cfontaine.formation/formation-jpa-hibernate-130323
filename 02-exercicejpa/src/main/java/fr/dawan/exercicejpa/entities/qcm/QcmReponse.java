package fr.dawan.exercicejpa.entities.qcm;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
@Entity
@Table(name="reponses")
public class QcmReponse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String respText;
    
    private boolean correct;
    
    @ManyToOne
    private QcmQuestion question; 

    public QcmReponse() {
    }

    public QcmReponse(String respText, boolean correct) {
        this.respText = respText;
        this.correct = correct;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRespText() {
        return respText;
    }

    public void setRespText(String respText) {
        this.respText = respText;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public QcmQuestion getQuestion() {
        return question;
    }

    public void setQuestion(QcmQuestion question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "QcmReponse [id=" + id + ", respText=" + respText + ", correct=" + correct + "]";
    }

}
