package fr.dawan.exercicejpa.entities.monuments;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name="coordonnee") 
public class Coordonee extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private double longitude;
    
    @Column(nullable = false)
    private double latitude;

    public Coordonee() {
    }

    public Coordonee(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Coordonee [longitude=" + longitude + ", latitude=" + latitude + "]";
    }
    
}
