package fr.dawan.exercicejpa.entities.monuments;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;
  

    @Column(length = 100, nullable = false)
    private String nom;

    @OneToMany(mappedBy="nation")
    private List<Monument> monuments=new ArrayList<>();
    

    public Nation() {
    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public List<Monument> getMonuments() {
        return monuments;
    }

    public void setMonuments(List<Monument> monuments) {
        this.monuments = monuments;
    }

    @Override
    public String toString() {
        return "Nation [nom=" + nom + "]";
    }
}

